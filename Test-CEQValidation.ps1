﻿<#
#CEQ Test cases.
@Date: 20-Jan-17
@Author: AnchitK
@Required: Powershell 4.0

This aims to provide methods to test CTQ feature added from 7.6 LTSR CU3

#>
#Import-Module "c:\ProgramData\Jonas\MetaInstallerAnalyticsModule.psm1";
Import-Module "c:\ProgramData\Jonas\MockObjects.psm1";

<#

.Name
Test-GenerationOfSesLogs


.Description
This method will search for the ses_* log created by the Metainstaller Analytics feature and on finding the log file it will
check it against a mock object.

#>
function Test-GenerationOfMetaInstallerLogs
{
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue";
    
    try
    {
        #sets the c:\programdata\.. location
        #find the file.
        Set-LocationToCTQ
        $sesFileObject = Get-LatestCTQLogfile "ses_*";
    }
    catch
    {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Write-Error "There was a error with message => $ErrorMessage . Failed at item => $FailedItem";
        Break
    }
    
    try
    {
        if(![string]::IsNullOrEmpty($sesFileObject))
        {
            $logContent = Get-Content $sesFileObject.FullName;
            Write-Debug "Ses log file found @ $($sesFileObject.FullName)"
        }
        else
        {
            Write-Debug "Ses log file not found." -ErrorAction Stop;
        }
    }
    catch
    {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Write-Error "There was a error with message => $ErrorMessage . Failed at item => $FailedItem";
        
    }
}


<#

.Name
Test-ValidationOfMetaInstallerLogs


.Description
This method will validate the Log file against a mock object for the VDA.

#>
function Test-Windows7FreshInstallVDAMetaInstallerLogs
{
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue";
    
    try
    {
        $sesFileObject = Get-LatestCTQLogfile "ses_*";
        $mockObject = Get-MockObject -component "vda" -os "windows7" -upgrade $false;
        if(![string]::IsNullOrEmpty($sesFileObject))
        {
            $logContent = Get-Content $sesFileObject.FullName | ConvertFrom-Json;
            Write-Debug "Ses log file found @ $($sesFileObject.FullName)"
        }
        else
        {
            Write-Debug "Ses log file not found.";
        }
    }
    catch
    {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Write-Error "There was a error with message => $ErrorMessage . Failed at item => $FailedItem";
        
    }
    #Dont know how to compare.
    if([string]::Compare($mockObject.app, $logContent.app, $true) -eq 0)
    {
        Write-Host "App is verified. Value= $($logContent.app)";
    }
    else 
    {
        Write-Host "App failed verification. log Value= $($logContent.app) VS Mock Value: $($mockObject.app)";
    }
    if($mockObject.osVer -eq $logContent.osVer)
    {
        Write-Host "osVer verified = $($logContent.osVer)";
    }
    else
    {
        Write-Host "osVer failed verification = $($logContent.osVer)";
    }
}