﻿cls
#$destinationComputer = "10.108.50.30";
$destinationComputer = "10.108.77.217";
$setupFolder = "CEQTesting";
$fileNameRegExp = "ses*";
$pass = convertto-securestring "citrix" -asplaintext -force;
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist "Administrator",$pass;

Copy-Item -Path '\\eng.citrite.net\ftl\homedirs\anchitk\Automation\CEQTesting' -Destination "\\$destinationComputer\c$" -Recurse -Force;

$remoteOutput = Invoke-Command -computername $destinationComputer -ScriptBlock {
#Dont change

Import-Module "c:\ProgramData\Jonas\Get-CEQLogs.psm1";

Set-LocationToCTQ;

$uuidFileObject = Get-LatestCTQUUIDFile;

$uuidToBeFetched = Read-CTQUUIDFile $uuidFileObject;


$CTQJsonObject  =  Get-CTQMetrics $uuidToBeFetched;

Read-CTQMetrics $CTQJsonObject;


} -Args $setupFolder -credential $cred #>
