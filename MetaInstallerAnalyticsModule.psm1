﻿<#
#CEQ Test cases.
@Date: 20-Jan-17
@Author: AnchitK
@Required: Powershell 4.0

This aims to provide methods to test CTQ feature added from 7.6 LTSR CU3

#>


<#

.Name
Set-LocationToCTQ


.Description
This method allows a user to set the location to C:\ProgramData\Citrix\CTQs\metainstaller.dat
by deafult. User can pass a parameter to change this value.

.PARAMETERS logpath
The path to metainstaller.dat folder. 

#>
function Set-LocationToCTQ
{
    Param(
    [parameter(Mandatory=$false)]
    [String] $logPath = "C:\ProgramData\Citrix\CTQs\metainstaller.dat"
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with param $logPath";
    Set-Location $logPath -ErrorAction Stop;

}

<#
.Name
Get-LatestCTQLogfile

.Description
Function recursively finds and then reads a log file and returns the file object from the currtent location. 
The user can pass regular expression or the file name.eg - ses*.

.PARAMETERS $fileName
Log file which needs to be read and returned.

#>
function Get-LatestCTQLogfile
{
    Param(
    [parameter(Mandatory=$true)]
    [String] $fileName = "ses_*"
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with param $fileName";
    $fileObject = Get-ChildItem -Recurse -File | Where-Object {$_.Name -like $fileName} | sort LastWriteTime -Descending;
    Write-Debug "Final value fo $fileObject";
    return $fileObject;
}

<#
.Name
Get-LatestCTQUUIDFile

.Description
Function recursively finds and then reads the file containing UUID and then return a list of file objects from the currtent location. 
The user can pass a string as the file name.eg - ses*.

.PARAMETERS fileNameRegExp
Regular expression to find the UUID patern

#>
function Get-LatestCTQUUIDFile
{
    Param(
    [parameter(Mandatory=$false)]
    $fileNameRegExp = [regex] '([a-zA-Z0-9_-]{32,}\.uuid$)'
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with params $fileNameRegExp";
    $fileObject = Get-ChildItem -Recurse -File | Where-Object {$_.Name -match $fileNameRegExp} | sort LastWriteTime -Descending;
    Write-Debug "File found $fileObject"
    $fileObject[0];
}


<#
.Name
Read-CTQUUIDFile

.Description

.PARAMETERS fileObject


#>
function Read-CTQUUIDFile
{
    Param(
    [parameter(Mandatory=$true)]
    $fileObject
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with params $fileObject";
    #supposdly the $fileObject.fullname attribute should return the full filepath with the filename.
    $uuid = Get-Content $fileObject.FullName;

    #Want to return the UUID only if it match the pattern
    if($uuid -match '([a-zA-Z0-9_-]{32,})')
    {
        Write-Verbose "returning $uuid";
    }
    else
    {
        Write-Error "File contents dont match UUID format of ([a-zA-Z0-9_-]{32,})";
        Write-Debug "$uuid";

    }
    $uuid;

}


<#
.Name
Read-MetaInstallerLogFile

.Description
The function reads the json log file saved at c:\programdata\metainstaller. returns a object with 
the Json content. 


.PARAMETERS fileObject


#>
function Read-MetaInstallerLogFile
{
    Param(
    [parameter(Mandatory=$true)]
    $fileObject
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with params $fileObject";
    #supposdly the $fileObject.fullname attribute should return the full filepath with the filename.
    $jsonObject = Get-Content $fileObject.FullName | ConvertFrom-Json ;

    Write-Debug $jsonObject;
    $jsonObject;

}

<#
.Name
Get-CTQMetrics

.Description
Function gets the CTQ logs from the CTQ server.

.PARAMETERS UUID
UUID to fetch results from CEIP server.

.PARAMETERS fileObject
CEIP server address. (Optional)

.Returns
Json data fromt the URL.
#>
function Get-CTQMetrics
{
    Param(
    [parameter(Mandatory=$true,Position=1)]
    [String] $UUID,
    [parameter(Mandatory=$false)]
    [String] $serverAddress = "http://rttf.citrite.net/feeds/manage/tuple"
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue with parameters $UUID and $serverAddress";

    $url = "$serverAddress/$UUID";
    Write-Debug "url: $url";
    
    $request = Invoke-WebRequest -ContentType "application/json" -URI $url;
    $web_client = new-object system.net.webclient
    $build_info=$web_client.DownloadString($url) | ConvertFrom-Json

    $build_info;
}

<#
.Name
Read-CTQMetrics

.Description
Function verifies the CTQ logs from the CTQ server.

.PARAMETERS file
UUID to fetch results from CTQserver.

.PARAMETERS fileObject
CTQ server address. (Optional)

#>
function Read-CTQMetrics
{
    Param(
        [parameter(Mandatory=$true,Position=1)]
        $ctqJsonObject,
        [parameter(Mandatory=$false,Position=2)]
        $ctqMockObject = $CTQMockObject
        )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue";

    

    try
    {    
        $result = ($ctqJsonObject.app -eq $ctqMockObject.app) 
        Write-Debug ($ctqJsonObject.app -eq $ctqMockObject.app)
        $result = $result -and ($ctqJsonObject.pver -eq $ctqMockObject.pver) 
        Write-Debug ($ctqJsonObject.pver -eq $ctqMockObject.pver) 
        $result = $result -and ($ctqJsonObject.prod -eq $ctqMockObject.prod)
        Write-Debug ($ctqJsonObject.prod -eq $ctqMockObject.prod)
    }
    catch
    {
        Write-Error " Read-CTQMetrics Could not read the values from URL. Result value: $result"
    }

    $result;
}


<#
.Description
Golabl mock variable to verify values from the JSON CTQ logs.
#>
$CTQMockObject = @{ 
    app = "XenDesktop"
    pver = "7.6.3000.7034"
    prod = "XenDesktop 7.6 LTSR CU3"
    };

<#
.Name
Convert-MetaInstallerMetricsToObject

.Description
Function converts a given Meta Installer Analytics Json to an array.

.PARAMETERS MetaInstallerAnalyticsJson
String version of Json to be processed.

#>
function Convert-MetaInstallerMetricsToObject
{
    Param(
        [parameter(Mandatory=$true,Position=1)]
        $ctqJsonObject
        )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue";

    try
    {    
        $jsonObject = ConvertFrom-Json $ctqJsonObject;
        
    }
    catch
    {
        Write-Error " Read-CTQMetrics Could not read the values from URL. Result value: $result"
    }

    $result;
}


<#
.Name
Verify-CEQMetrics

.Description
Function verifies the CEQ logs from the CEQ server.

.PARAMETERS UUID
UUID to fetch results from CEQ server.

.PARAMETERS fileObject
CEQ server address. (Optional)

#>
