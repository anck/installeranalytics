﻿<# 
.Name
Get-MockObjectVDAWindows7FreshInstall

.Description
This is a cusotm mock object for VDA Fresh Install.
ref: https://mindtouch.eng.citrite.net/Citrix_Insight_Services/Features/Analytics_%2F%2F_CEIP/Metainstaller/XA-1630_MetaInstaller_Analytics_-_CIS_Upload/Feature_Spec

app: string
    Type of MetaInstaller	One of: "XenDesktop", "XenApp"
prod: string
	Full product name, for example "XenDesktop 7.9.100"	Non empty
pver: string
	Product version, for example "7.9.100.42"	Non empty
bpvs: string
	Base Product Family, e.g. "19.0" for ColdFire, "12.0" for ?Jasper	 
uiMode: string
	UI mode: e.g. Normal, Passive, Quiet	non empty
uts: string
	Start timestamp, for example "2016-04-04T23:33:25.1293452Z"	Non empty. formatted as ISO 8601
ute: string
	End timestamp, for example "2016-04-04T23:43:25.1293452Z"	Non empty. formatted as ISO 8601
type: string
	Type of the install	One of: "Upgrade", "Install", "Remove", "Reconfigure"
mImg: bool
	Whether master image is being created	 
farm: string
	Farm identifier	 
rbt: number
	Number of reboots	Integer, 0 or bigger
osVer: string
	Operating system name version according to Microsoft, for example "WindowsServer2012"	 
osCode: string
	Operating system code according to Microsoft, for example "6.3.9600.0"	 
osBits: string
	Operating system bitness	One of: "x32", "x64"
osSP: string
	Operating system service pack ???	 
lang: string
	Operating system language, for example "en-US"	 
rbts: object[]
	Array of custom "reboot" objects, see below	 
pkgs: object[]
    Array of custom "package" objects, see below	 
}
#>
@{
    "app" = "XenDesktop";
    #"bpvs"="0.0";
    #"farm"="";
    #"iid"="998ce4fd-8f47-476f-a0a6-86f2729b9718";
    "lang"="en-US";
    "mImg"= $true;
    "mode"="Install";
    "osBits"="x64";
    "osCode"="6.1.7601.65536";
    "osSP"="Service Pack 1";
    "osVer"="Windows7";
    #"prod"="XenDesktop 7.6 LTSR CU3";
    #"pver"="7.6.3000.7034";
    "rbt"=1;
    #"res"="FailureAndRebootNeeded";  Needs to be added.
    "type"="Install";
    "uiMode"="Normal";
    #"ute"="2017-03-05T15:54:24.6266000Z";
    #"uts"="unknown";
    #"REvt"=null
}