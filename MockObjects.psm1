﻿<# 
.Name
Get-MockObjectVDAWindows7FreshInstall

.Description
This method returns the mock object depending on the parameters passed.

.Parameter component
Can take values: VDA,DDC,Studio,Director,StoreFront.

.Parameter OS
Can take values: Windows7,Windows2012, Windows81, Windows2008.

.Parameter Upgrade
Can take values: true, false.

ref: https://mindtouch.eng.citrite.net/Citrix_Insight_Services/Features/Analytics_%2F%2F_CEIP/Metainstaller/XA-1630_MetaInstaller_Analytics_-_CIS_Upload/Feature_Spec

#>
function Get-MockObject
{
    Param(
    [parameter(Mandatory=$true)]
    [string]$component,
    [parameter(Mandatory=$true)]
    [string]$os,
    [parameter(Mandatory=$true)]
    [boolean]$upgrade
    )
    $debugValue = $PSCmdlet.MyInvocation.MyCommand.Name;
    Write-Debug "$debugValue. Verifying $component for $os would be upgrading $upgrade";
    $mockObject = null;
    
    #fresh install
    if(!$upgrade)
    {
        $check = "$component$os";
        switch($check)
        {
            "vdawindows7"
            {
                $mockObject = Import-LocalizedData -FileName "Mock-VDAWindows7FreshInstall.psd1" -ErrorAction Stop;
                Write-Debug "$debugValue. Verifying $mockObject";
            }
            <#TODO: implement for windows 8.1
            "vdawindows81"
            {
                $MockData = Import-LocalizedData -FileName "Mock-VDAWindows81FreshInstall.psd1";
            }#>
            default
            {
                Write-Debug "$debugValue. Verifying $mockObject";
            }

        }
    }
    else #Upgrade scenario.
    {
        $check = "$component+$OS";
        switch($check)
        {
            #TODO: Implement for upgrade scenarios.
            default
            {
                $mockObject = Out-Null;
            }
        }

    }
    
    return $MockData;
    
}
